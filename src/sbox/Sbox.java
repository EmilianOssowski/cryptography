package sbox;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

public class Sbox {
    ArrayList<Integer> ints;
    ArrayList<Byte> bytes;
    ArrayList<String> sbxBits;
    FileInputStream file;
    AffinicFunctions af;
    ArrayList<String> xorTable;
    ArrayList<String> functions;

    public Sbox() throws IOException {
        try {
            file = new FileInputStream(new File(".\\resource\\sbox.SBX"));

            ints = new ArrayList<>();
            ints.add(file.read());
            while (file.read() != -1) {
                ints.add(file.read());
            }
            ints.remove(ints.size() - 1);
            //System.out.print(ints);
            bytes = new ArrayList<>();
            for (Integer i : ints) {
                bytes.add(i.byteValue());
            }
            sbxBits = new ArrayList<>();
            for (Byte b : bytes) {
                //System.out.println(toBinaryString(b));
                sbxBits.add(toBinaryString(b));
            }
            //System.out.print( sbxBits.size());

            file.close();

            af = new AffinicFunctions();

            functions();

            conditionSAK();


        } catch (
                FileNotFoundException e) {
            System.out.print("File not found");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void functions() {
        functions = new ArrayList<>();
        for (int i = 0; i < 8; i++) {
            functions.add(new String(""));
            for (String b : sbxBits) {
                functions.set(i, functions.get(i) + String.valueOf(b.charAt(i)));
            }
            System.out.println(functions.get(i));
            System.out.println("Balanced: " + isBalanced(functions.get(i)));
        }
        af.calculateDistance(functions.get(0));
    }


    String toBinaryString(int val) {
        StringBuilder sb = new StringBuilder();
        String str = Integer.toBinaryString(val);
        if (str.length() > 8) {
            sb.append(str.substring(str.length() - 8));
        } else {
            for (int i = 0; i < 8 - str.length(); i++) {
                sb.append('0');
            }
            sb.append(str);
        }
        return String.valueOf(sb);
    }

    boolean isBalanced(String s) {
        int ones = 0, zeros = 0;
        for (int i = 0; i < 256; i++) {
            if (s.charAt(i) == '1') {
                ones += 1;
            } else {
                zeros += 1;
            }
        }
        return zeros == ones;

    }

    void conditionSAK() {
        ArrayList<String> reversedFunctions = new ArrayList<>();
        for (String function : functions) {
            reversedFunctions.add(reverseFunction(function));
        }
        ArrayList<String> xorResults = new ArrayList<>();
        double probability = 0;
        for (int i = 0; i < 8; i++) {
            StringBuilder xorResult = new StringBuilder();
            for (int k = 0 ; k<256 ; k ++) {
                xorResult.append(Helper.xor(functions.get(i).charAt(k), reversedFunctions.get(i).charAt(k)));
            }
            System.out.println("XOR result "+i+ ":"+xorResult);
            System.out.println("Ones: " + Helper.countOnes(xorResult.toString()));
            double prob = (double) Helper.countOnes(xorResult.toString())/256;
            probability += prob;
            System.out.println("Probability: " +prob);
            xorResults.add(xorResult.toString());
        }
        probability = probability / 8;
        System.out.println("Final probability " + probability);

    }

    String reverseFunction(String function) {
        StringBuilder newFunction = new StringBuilder();
        //System.out.println(function);
        for (int i = 1; i < 256; i += 2) {
            newFunction.append(function.charAt(i));
            newFunction.append(function.charAt(i - 1));
        }
        //System.out.println(newFunction);
        return newFunction.toString();
    }

    String reverseDoubleFunction(String function) {
        StringBuilder newFunction = new StringBuilder();
        System.out.println(function);
        for (int i = 3; i < 256; i += 4) {
            newFunction.append(function.charAt(i - 1));
            newFunction.append(function.charAt(i));
            newFunction.append(function.charAt(i - 3));
            newFunction.append(function.charAt(i - 2));
        }
        System.out.println(newFunction);
        return newFunction.toString();
    }
}
