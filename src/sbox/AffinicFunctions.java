package sbox;

import java.util.ArrayList;

public class AffinicFunctions {
    String[] possibleFunctions;
    ArrayList<String> affinicFunctions;
    ArrayList<Integer> distance;

    public AffinicFunctions() {
        affinicFunctions = new ArrayList<>();
        combinate();
    }

    void combinate() {

        possibleFunctions = new String[8];

        for (int i = 0; i < 8; i++) {
            possibleFunctions[i] = "";
        }

        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                for (int k = 0; k < 2; k++) {
                    for (int l = 0; l < 2; l++) {
                        for (int m = 0; m < 2; m++) {
                            for (int n = 0; n < 2; n++) {
                                for (int o = 0; o < 2; o++) {
                                    for (int p = 0; p < 2; p++) {
                                        possibleFunctions[0] += i;
                                        possibleFunctions[1] += j;
                                        possibleFunctions[2] += k;
                                        possibleFunctions[3] += l;
                                        possibleFunctions[4] += m;
                                        possibleFunctions[5] += n;
                                        possibleFunctions[6] += o;
                                        possibleFunctions[7] += p;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        int it = 7;
        for (String s : possibleFunctions) {
            System.out.println("X" + it + ": " + s);
            it -= 1;
        }
        generateAffinicFunctions();
    }

    void generateAffinicFunctions() {

        int j = 0;
        //0 and 1{
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 256; i++) {
                sb.append('0');
            }
            j++;
            System.out.println(j + " 0 :"  +sb);
            affinicFunctions.add(sb.toString());
        }
//        {
//            StringBuilder sb = new StringBuilder();
//            for (int i = 0; i < 256; i++) {
//                sb.append('1');
//            }
//            j++;
//            System.out.println(j + " 1 :" + sb);
//            affinicFunctions.add(sb.toString());
//        }
        //x
        for (int x0 = 0; x0 < 8; x0++) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 256; i++) {
                sb.append(possibleFunctions[x0].charAt(i));
            }
            j++;
            System.out.println(j + " x" + x0 +  ":"  +sb);
            affinicFunctions.add(sb.toString());

        }
        //x0x1

        for (int x0 = 0; x0 < 8; x0++) {
            for (int x1 = 1; x1 < 8; x1++) {
                if (x1 != x0 && x1 > x0) {

                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < 256; i++) {

                        sb.append(Helper.xor(possibleFunctions[x0].charAt(i), possibleFunctions[x1].charAt(i)));
                    }
                    affinicFunctions.add(sb.toString());
                    j++;
                    System.out.println(j + " x" + x0 + "x" + x1 + ":" + sb);
                }
            }

        }
        //x0x1x2
        for (int x0 = 0; x0 < 8; x0++) {
            for (int x1 = 1; x1 < 8; x1++) {
                for (int x2 = 2; x2 < 8; x2++) {
                    if (x1 != x0 && x1 > x0 &&
                            x2 != x1 && x2 > x1) {

                        StringBuilder sb = new StringBuilder();
                        for (int i = 0; i < 256; i++) {

                            sb.append(Helper.xor(possibleFunctions[x0].charAt(i), possibleFunctions[x1].charAt(i), possibleFunctions[x2].charAt(i)));
                        }
                        affinicFunctions.add(sb.toString());
                         j++;
                         System.out.println(j + " x" + x0 + "x" + x1 + "x" + x2 + ":" + sb);
                    }
                }
            }
        }
        //x0x1x2x3
        for (int x0 = 0; x0 < 8; x0++) {
            for (int x1 = 1; x1 < 8; x1++) {
                for (int x2 = 2; x2 < 8; x2++) {
                    for (int x3 = 3; x3 < 8; x3++) {
                        if (x1 != x0 && x1 > x0 &&
                                x2 != x1 && x2 > x1 &&
                                x3 != x2 && x3 > x2) {

                            StringBuilder sb = new StringBuilder();
                            for (int i = 0; i < 256; i++) {

                                sb.append(Helper.xor(possibleFunctions[x0].charAt(i), possibleFunctions[x1].charAt(i), possibleFunctions[x2].charAt(i), possibleFunctions[x3].charAt(i)));
                            }
                            affinicFunctions.add(sb.toString());
                            j++;
                            System.out.println(j + " x" + x0 + "x"+ x1 + "x" + x2 + "x" + x3 + ":" + sb);
                        }
                    }
                }
            }
        }
        //x0x1x2x3x4
        for (int x0 = 0; x0 < 8; x0++) {
            for (int x1 = 1; x1 < 8; x1++) {
                for (int x2 = 2; x2 < 8; x2++) {
                    for (int x3 = 3; x3 < 8; x3++) {
                        for (int x4 = 4; x4 < 8; x4++) {
                            if (x1 != x0 && x1 > x0 &&
                                    x2 != x1 && x2 > x1 &&
                                    x3 != x2 && x3 > x2 &&
                                    x4 != x3 && x4 > x3) {

                                StringBuilder sb = new StringBuilder();
                                for (int i = 0; i < 256; i++) {

                                    sb.append(Helper.xor(possibleFunctions[x0].charAt(i), possibleFunctions[x1].charAt(i), possibleFunctions[x2].charAt(i), possibleFunctions[x3].charAt(i), possibleFunctions[x4].charAt(i)));
                                }
                                affinicFunctions.add(sb.toString());
                                j++;
                                System.out.println(j + " x" + x0 + "x" + x1 + "x" + x2 + "x" + x3 + "x" + x4 + ":" + sb);
                            }
                        }
                    }
                }
            }
        }
        //x0x1x2x3x4x5
        for (int x0 = 0; x0 < 8; x0++) {
            for (int x1 = 1; x1 < 8; x1++) {
                for (int x2 = 2; x2 < 8; x2++) {
                    for (int x3 = 3; x3 < 8; x3++) {
                        for (int x4 = 4; x4 < 8; x4++) {
                            for (int x5 = 5; x5 < 8; x5++) {
                                if (x1 != x0 && x1 > x0 &&
                                        x2 != x1 && x2 > x1 &&
                                        x3 != x2 && x3 > x2 &&
                                        x4 != x3 && x4 > x3 &&
                                        x5 != x4 && x5 > x4) {

                                    StringBuilder sb = new StringBuilder();
                                    for (int i = 0; i < 256; i++) {

                                        sb.append(Helper.xor(possibleFunctions[x0].charAt(i), possibleFunctions[x1].charAt(i), possibleFunctions[x2].charAt(i), possibleFunctions[x3].charAt(i), possibleFunctions[x4].charAt(i), possibleFunctions[x5].charAt(i)));
                                    }
                                    affinicFunctions.add(sb.toString());
                                    j++;
                                    System.out.println(j + " x" + x0 + "x" + x1 + "x" + x2 + "x" + x3 + "x" + x4+ "x" + x5 + ":" + sb);
                                }
                            }
                        }
                    }
                }
            }
        }
        //x0x1x2x3x4x5x6
        for (int x0 = 0; x0 < 8; x0++) {
            for (int x1 = 1; x1 < 8; x1++) {
                for (int x2 = 2; x2 < 8; x2++) {
                    for (int x3 = 3; x3 < 8; x3++) {
                        for (int x4 = 4; x4 < 8; x4++) {
                            for (int x5 = 5; x5 < 8; x5++) {
                                for (int x6 = 6; x6 < 8; x6++) {
                                    if (x1 != x0 && x1 > x0 &&
                                            x2 != x1 && x2 > x1 &&
                                            x3 != x2 && x3 > x2 &&
                                            x4 != x3 && x4 > x3 &&
                                            x5 != x4 && x5 > x4 &&
                                            x6 != x5 && x6 > x5) {

                                        StringBuilder sb = new StringBuilder();
                                        for (int i = 0; i < 256; i++) {

                                            sb.append(Helper.xor(possibleFunctions[x0].charAt(i), possibleFunctions[x1].charAt(i), possibleFunctions[x2].charAt(i), possibleFunctions[x3].charAt(i), possibleFunctions[x4].charAt(i), possibleFunctions[x5].charAt(i), possibleFunctions[x6].charAt(i)));
                                        }
                                        affinicFunctions.add(sb.toString());
                                        j++;
                                        System.out.println(j + " x" + x0 + "x" + x1 + "x" + x2 + "x" + x3 + "x" + x4 + "x" + x5  + "x" + x6 + ":" + sb);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        //x0x1x2x3x4x5x6x7
        for (int x0 = 0; x0 < 8; x0++) {
            for (int x1 = 1; x1 < 8; x1++) {
                for (int x2 = 2; x2 < 8; x2++) {
                    for (int x3 = 3; x3 < 8; x3++) {
                        for (int x4 = 4; x4 < 8; x4++) {
                            for (int x5 = 5; x5 < 8; x5++) {
                                for (int x6 = 6; x6 < 8; x6++) {
                                    for (int x7 = 7; x7 < 8; x7++) {
                                        if (x1 != x0 && x1 > x0 &&
                                                x2 != x1 && x2 > x1 &&
                                                x3 != x2 && x3 > x2 &&
                                                x4 != x3 && x4 > x3 &&
                                                x5 != x4 && x5 > x4 &&
                                                x6 != x5 && x6 > x5 &&
                                                x7 != x6 && x7 > x6) {

                                            StringBuilder sb = new StringBuilder();
                                            for (int i = 0; i < 256; i++) {

                                                sb.append(Helper.xor(possibleFunctions[x0].charAt(i), possibleFunctions[x1].charAt(i), possibleFunctions[x2].charAt(i), possibleFunctions[x3].charAt(i), possibleFunctions[x4].charAt(i), possibleFunctions[x5].charAt(i), possibleFunctions[x6].charAt(i), possibleFunctions[x7].charAt(i)));
                                            }
                                            affinicFunctions.add(sb.toString());
                                            j++;
                                            System.out.println(j + " x" + x0 + "x" + x1 + "x" + x2 + "x" + x3 + "x" + x4 + "x" + x5 + "x" + x6 + "x" + x7 + ":" + sb);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        System.out.println(affinicFunctions.size());
    }

    public ArrayList<String> getAffinicFunctions(){
        return affinicFunctions;
    }


    void calculateDistance(String function) {
        distance = new ArrayList<>();
        for (String af : affinicFunctions) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 256; i++) {
                sb.append(Helper.xor(function.charAt(i), af.charAt(i)));

            }
            distance.add(countOnes(sb.toString()));
        }
        System.out.println(findSmallest(distance));
    }
    int countOnes(String s) {
        int res = 0;
        for (char c : s.toCharArray()) {
            if (c == '1') res += 1;
        }
        return res;
    }
    int findSmallest(ArrayList<Integer> arr){
        int smallest = 256;
        for(int i : arr){
            if (i < smallest) smallest = i;
        }
        return smallest;
    }

    public String[] getPossibleFunctions(){
        return possibleFunctions;
    }
}
