package sbox;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Helper {
    private static Random random = new Random();

    public static int randInt(int min, int max) {
        return random.nextInt((max - min) + 1) + min;
    }

    public static char xor(char x0, char x1) {
        return boolToChar(charToBool(x0) ^ charToBool(x1));
    }

    public static char xor(char x0, char x1, char x2) {
        return boolToChar(charToBool(x0) ^ charToBool(x1) ^ charToBool(x2));
    }

    public static char xor(char x0, char x1, char x2, char x3) {
        return boolToChar(charToBool(x0) ^ charToBool(x1) ^ charToBool(x2) ^ charToBool(x3));
    }

    public static char xor(char x0, char x1, char x2, char x3, char x4) {
        return boolToChar(charToBool(x0) ^ charToBool(x1) ^ charToBool(x2) ^ charToBool(x3) ^ charToBool(x4));
    }

    public static char xor(char x0, char x1, char x2, char x3, char x4, char x5) {
        return boolToChar(charToBool(x0) ^ charToBool(x1) ^ charToBool(x2) ^ charToBool(x3) ^ charToBool(x4) ^ charToBool(x5));
    }

    public static char xor(char x0, char x1, char x2, char x3, char x4, char x5, char x6) {
        return boolToChar(charToBool(x0) ^ charToBool(x1) ^ charToBool(x2) ^ charToBool(x3) ^ charToBool(x4) ^ charToBool(x5) ^ charToBool(x6));
    }

    public static char xor(char x0, char x1, char x2, char x3, char x4, char x5, char x6, char x7) {
        return boolToChar(charToBool(x0) ^ charToBool(x1) ^ charToBool(x2) ^ charToBool(x3) ^ charToBool(x4) ^ charToBool(x5) ^ charToBool(x6) ^ charToBool(x7));
    }

    public static int xor(List<Integer> values) {
        int lastIndex = values.size() - 1;
        if (lastIndex == 0) return values.get(lastIndex);
        else {
            return boolToInt(intToBool(values.get(lastIndex)) ^ intToBool(xor(values.subList(0, lastIndex))));
        }
    }

    static boolean intToBool(int i) {
        switch (i) {
            case 1:
                return true;
            case 0:
                return false;
            default:
                throw new IllegalArgumentException("Must be either 0 or 1.");
        }
    }

    static int boolToInt(boolean b) {
        if (b) {
            return 1;
        } else if (!b) {
            return 0;
        } else {
            throw new IllegalArgumentException("Must be either true or false.");
        }
    }

    static boolean charToBool(char c) {
        // c = Character.toLowerCase(c);
        switch (c) {
            case '1':
                return true;
            case '0':
                return false;
            default:
                throw new IllegalArgumentException("Must be either '0' or '1'.");
        }
    }

    static char boolToChar(boolean b) {
        // c = Character.toLowerCase(c);
        if (b) {
            return '1';
        } else if (b == false) {
            return '0';
        } else {
            throw new IllegalArgumentException("Must be either true or false.");
        }
    }

    static int countOnes(String s) {
        int res = 0;
        for (char c : s.toCharArray()) {
            if (c == '1') res += 1;
        }
        return res;
    }

}
