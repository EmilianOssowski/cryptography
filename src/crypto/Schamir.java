package crypto;


import sbox.Helper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Random;

public class Schamir {
    private int s, p, t, n;
    private Random random = new Random();
    private HashMap<Integer, Integer> aList = new HashMap<>();
    private HashMap<Integer, Integer> keyList = new HashMap<>();

    private HashMap<Integer, Integer> polynomial = new HashMap<>();

    public Schamir(int t, int n) {  // t - wymagane do odtworzenia, n - ilosc fragmentó
        this.t = t;
        this.n = n;
        p = 1459;
//        p = Helper.randInt(s, 10000);
        s = random.nextInt(p-1);

        System.out.println("Sekret: " + s);
        polynomial.put(0, s);
        for (int i = 1; i < t; i++) {
            polynomial.put(i, random.nextInt(p));
        }
        createKeys();
        printPolynomial();
        printKeys();
        HashMap<Integer, Integer> test = new HashMap();
        test.put(1, this.keyList.get(1));
        test.put(2, this.keyList.get(2));
        test.put(3, this.keyList.get(3));
        test.put(4, this.keyList.get(4));
        test.put(5, this.keyList.get(5));
        getSecret(test);
    }

    private void createKeys() {

        for (int i = 1; i <= n; i++) {
            double result = 0;
            for (int k : polynomial.keySet()) {
                result = result + polynomial.get(k) * Math.pow(i, k);
            }
            result = result % p;
            keyList.put(i, (int) result);
        }

    }

    public void getSecret(HashMap<Integer, Integer> keys) {
        System.out.println(keys);

        double result = 0;
        for (int i : keys.keySet()) {
            double secret = keys.get(i);
            for (int j : keys.keySet()) {
                if (i != j) {
                    secret = secret * ((-1) * j)/ (i - j) ;
                }
            }
            secret = secret % p;
//            result = result + secret ;

            System.out.println("Wyraz wolny " + secret);
            if (result < 0) {
                result += p;
            }
            result = result + secret;
        }
        if (result < 0) {
            result += p;
        }else if(result > p){
            result -= p;
        }
        System.out.println(result);

    }

    private void printKeys() {
        for (int i : keyList.keySet()) {
            System.out.println("Key (" + i + "," + keyList.get(i) + ")");
        }
    }


    private void printPolynomial() {
        StringBuilder sb = new StringBuilder();
        for (int i : polynomial.keySet()) {
            sb.append(polynomial.get(i));
            sb.append("x^");
            sb.append(i);
            sb.append("+");
        }
        sb.delete(sb.length() - 1, sb.length());
        System.out.println(sb.toString());
    }

//    private ArrayList<Integer> lagrangeInterpolation(Collection<Integer> _tabS, int _t) {
//
//        ArrayList<Integer> _tabX = new ArrayList<Integer>();
//        for (int i = 1; i <= _tabS.size(); i++)
//            _tabX.add(i);
//
//        ArrayList<Integer> _arrayX = new ArrayList<Integer>();
//        for (int k = 1; k < _tabS.size(); k++) {
//            for (int j = 1; j < _tabS.size(); j++) {
//                if (j != k) {
//                    _arrayX.add(_t);
//                    _arrayX.add((-1) * _tabX.get(j));
//                    _arrayX.add(_tabX.get(k) - _tabX.get(j));
//                }
//            }
//        }
//        System.out.println(_arrayX);
//        return _arrayX;
//    }


//    public Schamir(int t){
//        Random random = new Random();
//        this.p = random.nextInt(Integer.MAX_VALUE);
//        this.s = random.nextInt(p-1);
//        System.out.println("Sekret: "+ s);
//        for(int i = 1; i<=t-1; i++){
//            aList.put(i, random.nextInt(p));
//        }
//        for(int i= 1; i<=aList.size(); i++){
//            createPolymional(i);
//        }
//        printKeys();
//    }
}