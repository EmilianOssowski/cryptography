package crypto;



import sbox.Helper;

import java.util.ArrayList;
import java.util.List;

public class LFSR {
    private List<Integer> register = new ArrayList<>();
    private ArrayList<Integer> shiftIndexes = new ArrayList<>();

    public LFSR(){
        shiftIndexes.add(3);
        shiftIndexes.add(7);
        shiftIndexes.add(13);
        shiftIndexes.add(26);
        shiftIndexes.add(41);
        for(int i = 0 ; i < 50 ; i++){
            register.add(Helper.randInt(0,1));
        }
//        System.out.print("Register : ");
//        System.out.println(register);

    }
    private int shift() {
        ArrayList<Integer> xorValues = new ArrayList<>();
        shiftIndexes.forEach(x -> xorValues.add(register.get(x)));
        int val = Helper.xor(xorValues);
        register = register.subList(1, register.size());
        register.add(val);
        return val;
    }
    public int getValue(){
        int val = shift();
//        System.out.println("Wartosć: " + val);
        return val;
    }
    public ArrayList<Integer> getValue(int iterations){
        ArrayList<Integer> result = new ArrayList<>();
        for (int i = 0 ; i< iterations; i++){
            result.add(getValue());
        }
        return result;
    }


}
