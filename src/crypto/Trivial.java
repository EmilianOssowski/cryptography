package crypto;


import java.util.ArrayList;
import java.util.Random;

public class Trivial {
    private int k,s,n;
    private ArrayList<Integer> keys = new ArrayList<>();
    private Random random = new Random();

    public Trivial(int s, int n, int k) {
        this.k = k;
        this.n = n;
        this.s = s;
        this.initKeys();
    }

    public Trivial(int secret, int partCount) {
        this.s = secret;
        this.n = partCount;
        this.k = 10000;
        this.initKeys();
    }

    private void initKeys() {
        for (int i = 0; i < n - 1; i++) {
            int key = this.random.nextInt(k - 1);
            this.keys.add(key);
        }
        int keyN = s;
        for (int key: keys ) {
            keyN = keyN - key;
        }
        keyN = keyN % k;
        this.keys.add(keyN);
        printKeys();
    }
    public void getSecret(){
        int s = 0;
        for (int k: keys){
            s = s +k;
        }
        s = s%k;
        System.out.println("Secret " +s);
    }
    private void printKeys(){
        System.out.println(this.keys);
    }


}
